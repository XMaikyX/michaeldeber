﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseUleam.Almacenamiento
{
    class SeleccionarTutoria
    {
        public int ID { get; set; }
        public DateTime Fecha { get; set; }
        public string Aula { get; set; }
        public string Asignatura { get; set; }
        public string Tema { get; set; }
        public string Docente { get; set; }

    }
}
