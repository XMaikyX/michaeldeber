﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseUleam.Almacenamiento
{
    class EntityFrameworkContext: DbContext
    {
        private const string connectionString =
            "Server=.\\MICHAEL; Database= BaseUleam; Trusted_Connection= True;";
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(connectionString);

        }
        public DbSet<Administrador> Administradors { get; set; }
        public DbSet<Docentes> Docentess { get; set; }
        public DbSet<Alumnos> Alumnoss { get; set; }
        public DbSet<ListaAula> listaAulas { get; set; }
        public DbSet<Tutoria> Tutorias { get; set; }
        public DbSet<SeleccionarTutoria> SeleccionarTutorias { get; set; }

    }
}
