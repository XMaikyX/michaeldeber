﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BaseUleam.Almacenamiento;

namespace BaseUleam
{
    class Program
    {
        static void Main(string[] args)
        {
            
            int opcion1, opcion2, opcion3;
            do
            {
                Console.Clear();
                Console.WriteLine("====================UNIVERSIDAD LAICA ELOY ALFARO DE MANABÍ=================");
                Console.WriteLine("=========FACULTAD DE CIENCIAS INFORMÁTICAS=======TECNOLOGÍAS DE LA INFORMACIÓN======");
                Console.WriteLine("SISTEMA DE CONTROL DE REGISTRO DE TUTORÍAS ACADÉMICAS");
                Console.WriteLine("Bienvenido");
                Console.WriteLine("Como que tipo de Usuario desea ingresar");
                Console.WriteLine("\n 1.- Estudiante\n" +
                                  "\n 2.- Docente\n" +
                                  "\n 3.- Administrador\n" +
                                  "\n 4.- Salir \n Opcion:  ");

                opcion1 = Convert.ToInt32(Console.ReadLine());
                switch (opcion1)
                {
                    case 1:
                        do
                        {
                            Console.Clear();
                            Console.WriteLine("Bienvenido a la ventana del usuario Estudiante \n");
                            Console.WriteLine("\n 1.- Elegir Tutoría " +
                                              "\n 2.- Regresar \nOpcion:");
                            opcion2 = Convert.ToInt32(Console.ReadLine());

                            switch (opcion2)
                            {
                                case 1:
                                    Console.Clear();
                                    Console.WriteLine("Lista de las Tutorías \n");
                                    //ElegirTutorias();
                                    //IngresarProfesor();
                                    Login();
                                    Console.WriteLine("Presione la tecla Enter para continuar");
                                    Console.ReadLine();
                                    break;
                                case 2:
                                    break;
                            }
                        } while (opcion2 != 2);
                        break;

                    case 2:
                        do
                        {
                            Console.Clear();
                            Console.WriteLine("Bienvenido a la ventana del usuario Docente");
                            Console.WriteLine("\n 1.-Crear Tutoría" +
                                              "\n 2.- Regresar");
                            opcion2 = Convert.ToInt32(Console.ReadLine());
                            switch (opcion2)
                            {
                                case 1:
                                    Console.WriteLine("Información detallada de las tutorías creadas \n");
                                    CrearTutoria();
                                    Console.WriteLine("Lista de Tutorias");
                                    ListaDeTutorias();
                                    Console.WriteLine("Presione cualquier tecla para continuar");
                                    Console.ReadLine();
                                    break;
                                case 2:
                                    break;
                            }

                        } while (opcion2 != 2);
                        break;
                    case 3:
                        do
                        {
                            Console.WriteLine("Bienvenido a la ventana del usuario Administrador \n");
                            Console.WriteLine("Acceder al Contenido ");
                            Console.WriteLine("=============Administrar Usuarios===============");
                            Console.WriteLine("\n 1.- Estudiante\n" +
                                              "\n 2.- Docente\n" +
                                              "\n 3.- Regresar");
                            opcion3 = Convert.ToInt32(Console.ReadLine());
                            switch (opcion3)
                            {
                                case 1:
                                    Console.WriteLine("Visualización del Contenido del Estudiante \n");
                                    Console.WriteLine("Presione cualquier tecla para continuar");
                                    Console.ReadLine();
                                    break;
                                case 2:
                                    Console.WriteLine("Visualización del Contenido del Docente \n");
                                    Console.WriteLine("Presione cualquier tecla para continuar");
                                    Console.ReadLine();
                                    break;
                                case 3:
                                    break;
                            }

                        } while (opcion3 != 3);
                        break;
                }
            } while (opcion1 != 4);

        }

        static void ListadoProfesores()
        {
            using (var db = new EntityFrameworkContext())
            {
                List<Docentes> listadoProfesores = db.Docentess.ToList();
                foreach (var profesor in listadoProfesores)
                {
                    Console.WriteLine(profesor.Apellido, profesor.Nombre);
                }
            }
        }
        static void IngresarProfesor()
        {
            using (var db = new EntityFrameworkContext())
            {
                Docentes docentes = new Docentes();
                Console.WriteLine("Ingrese el nombre del docente");
                docentes.Nombre = Console.ReadLine();
                Console.WriteLine("Ingrese el apellido del docente");
                docentes.Apellido = Console.ReadLine();
                Console.WriteLine("Ingrese su numero de cedula");
                docentes.Cedula = Console.ReadLine();
                Console.WriteLine("Su correo es el siguiente");
                docentes.Correo = docentes.Cedula + "@uleam.ec";
                Console.WriteLine(docentes.Correo);
                Console.WriteLine("Asigne una contraseña a su cuenta");
                docentes.Contrasenia = Console.ReadLine();

                db.Add(docentes);
                db.SaveChanges();

            }
            return;

        }
        static void CrearTutoria()
        {
            using (var db = new EntityFrameworkContext())
            {
                Tutoria tutoria = new Tutoria();
                SeleccionarTutoria seleccionarTutoria = new SeleccionarTutoria();

                Console.WriteLine("Proceda a escribir la fecha y hora de la Tutoria");
                Console.WriteLine("Escriba el Año ");
                string anio = Console.ReadLine();
                Console.WriteLine("Escriba el Mes");
                string mes = Console.ReadLine();
                Console.WriteLine("Escriba el Dia");
                string dia = Console.ReadLine();
                Console.WriteLine("Escriba la hora");
                string hora = Console.ReadLine();
                DateTime crearfecha = new DateTime(int.Parse(anio), int.Parse(mes), int.Parse(dia), int.Parse(hora), 0, 0);
                tutoria.Fecha = crearfecha;
                seleccionarTutoria.Fecha = crearfecha;

                Console.WriteLine("Ingrese el Aula a utilizar");
                tutoria.Aula = Console.ReadLine();
                seleccionarTutoria.Aula = tutoria.Aula;
                Console.WriteLine("Ingrese la Asignatura");
                tutoria.Asignatura = Console.ReadLine();
                seleccionarTutoria.Asignatura = tutoria.Asignatura;
                Console.WriteLine("Ingrese el Tema");
                tutoria.Tema = Console.ReadLine();
                seleccionarTutoria.Tema = tutoria.Tema;
               // seleccionarTutoria.Docente = resultado.nombre;
                Console.WriteLine("\nSe asignó tutoría para:");
                Console.WriteLine(tutoria.Fecha + " " + tutoria.Aula + " " + tutoria.Asignatura + "\n" + tutoria.Tema);

                db.Add(tutoria);
                db.SaveChanges();
            }
            return;
        }
        static void ListaDeTutorias()
        {
            using (var db = new EntityFrameworkContext())
            {
                List<Tutoria> ListaDeTutorias = db.Tutorias.ToList();
                foreach (var Tutoria in ListaDeTutorias)
                {
                    Console.WriteLine( "Fecha: "+Tutoria.Fecha +"  Aula: "+ Tutoria.Aula +"  Asignatura: "+ Tutoria.Asignatura+ "  Tema:"+ Tutoria.Tema);
                }
            }
        }
        static void ElegirTutorias()
        {
            using (var db = new EntityFrameworkContext())
            {
                List<SeleccionarTutoria> SeleccionarTutorias = db.SeleccionarTutorias.ToList();
                foreach (var seleccionarTutoria in SeleccionarTutorias)
                {
                    Console.WriteLine("Fecha: " +seleccionarTutoria.Fecha + "  Aula: " + seleccionarTutoria.Aula + "  Asignatura: " + seleccionarTutoria.Asignatura + "  Tema:" + seleccionarTutoria.Tema+"   Docente"+seleccionarTutoria.Docente);
                }
            }
        }

        static void Login()
        {
            using (var db = new EntityFrameworkContext())
            {
                Console.WriteLine("Escriba el Correo del usuario");
                var docentes = (from resultado in db.Docentess
                                where resultado.Correo == Console.ReadLine()
                                select resultado).FirstOrDefault<Docentes>();
                Console.WriteLine("Escriba la contraseña del usuario");
                if (docentes.Contrasenia == Console.ReadLine() )
                {
                    Console.WriteLine(" Bienvenido {0} {1} {2} ", docentes.Apellido,docentes.Nombre,docentes.Cedula);
                }
                else
                {
                    Console.WriteLine("No existe usuario");
                }
                
            }

        }

    }


}



